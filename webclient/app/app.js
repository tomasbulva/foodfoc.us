(function() {
  'use strict';

  angular
    .module('app',['ui.router', 'satellizer'])
    .config(appConfigFce)
    .run(myUtilsFce);

appConfigFce.$inject = ['$logProvider', '$locationProvider', '$httpProvider', '$authProvider'];
function appConfigFce($logProvider,$locationProvider, $httpProvider, $authProvider){
  $logProvider.debugEnabled(true);
  //$authProvider.httpInterceptor = true;
  
  $authProvider.facebook({
      clientId: '1519270458363727',
      url: 'http://foodfoc.us.dev:3000/auth/facebook'
    });

    // $authProvider.google({
    //   clientId: 'Google Client ID'
    // });

    // $authProvider.github({
    //   clientId: 'GitHub Client ID'
    // });

    // $authProvider.linkedin({
    //   clientId: 'LinkedIn Client ID'
    // });

    // $authProvider.yahoo({
    //   clientId: 'Yahoo Client ID / Consumer Key'
    // });

    // $authProvider.live({
    //   clientId: 'Microsoft Client ID'
    // });
}

myUtilsFce.$inject = ['$rootScope', '$state', '$stateParams', '$log', '$location', '$timeout', '$window', '$auth', '$http'];
function myUtilsFce($rootScope, $state, $stateParams, $log, $location, $timeout, $window, $auth, $http){
      $log.debug('[angular.run] $state', $state);
      $rootScope.$on("$stateChangeError", console.log.bind(console));

      $rootScope.api = "http://foodfoc.us.dev:3000";

      // somewhere else
      $rootScope.$on('$stateNotFound', 
      function(event, unfoundState, fromState, fromParams){ 
          console.log(unfoundState.to); // "lazy.state"
          console.log(unfoundState.toParams); // {a:1, b:2}
          console.log(unfoundState.options); // {inherit:false} + default options
      });

      $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams){
          $rootScope.statename = $state.current.name;

          $log.debug('[angular.run $stateChangeSuccess] toState', toState);
          $log.debug('[angular.run $stateChangeSuccess] toParams', toParams);
          $log.debug('[angular.run $stateChangeSuccess] toParams.style',toParams.style);
          $log.debug('[angular.run $stateChangeSuccess] toParams.category',toParams.category);
          $log.debug("[angular.run $stateChangeSuccess] toParams.scrollTo",toParams.scrollTo);
      });               
}

///////////// end closure  
})();

