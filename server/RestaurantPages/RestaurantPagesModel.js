var mongoose = require('mongoose');

var restaurantPagesSchema = new mongoose.Schema({
  name: String,
  order: String,
  type: String,
  copy: String,
  state: String,
  herePhoto: Schema.Types.ObjectId
});

module.exports = mongoose.model('RestaurantPages', restaurantPagesSchema);