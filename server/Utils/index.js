
module.exports = {
	generateToken: function (lenght) {
		var len = (lenght === undefined)? 32 : lenght;
		var text = "";
		var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

		for( var i=0; i < len; i++ ) {
			text += possible.charAt(Math.floor(Math.random() * possible.length));
		}
		return text;
	},
	globalHeaders: function(res) {
	    return res.setHeader("X-Powered-By", "FoodFoc.us");
	},
	slugify: function(unsafe){
	    unsafe = unsafe.toLowerCase();
	    unsafe = unsafe.replace(/[^a-z0-9]+/g, '-');
	    unsafe = unsafe.replace(/^-|-$/g, '');
	    return unsafe;
	},
	checkValidDbId: function(id){
		var idRex = /^[a-f\d]{24}$/i;
		return (id.match(idRex))? true : false;
	}
};