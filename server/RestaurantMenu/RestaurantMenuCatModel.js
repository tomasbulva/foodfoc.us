var mongoose = require('mongoose');

var restaurantMenuCatSchema = new mongoose.Schema({
  name: String,
  description: String,
  order: String,
  items: [mongoose.Schema.Types.ObjectId],
  menu: mongoose.Schema.Types.ObjectId
});

module.exports = mongoose.model('RestaurantMenuCat', restaurantMenuCatSchema);
