(function() {
	'use strict';  

	angular
		.module('app')
		.controller('DashboardHeaderController', headerControllerFce)

		headerControllerFce.$inject = ['$scope','$rootScope', '$http', '$state', '$log', '$auth'];
		function headerControllerFce($scope, $rootScope, $http, $state, $log, $auth){
			$log.debug("*************** DashboardHeaderController ****************");
			
			// if($auth.isAuthenticated()){
			// 	$state.go('dashboard')
			// }else{
				$scope.logout = function(provider) {
					console.log("[DashboardHeaderController] logout clicked")
					$auth.logout();
					$state.go('home');
			    };

		  	//}

		};

///////////// end closure  
})();