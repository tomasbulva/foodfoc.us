(function () {
/*global angular:false */
'use strict';

angular
	.module('app')
	.factory('API', modelFce);
	
	modelFce.$inject = ['$http','$rootScope','$log'];
	
	function modelFce($http,$rootScope,$log){
		return {
			//////////// getters
			getCurrUser: getCurrUserData
			
			//////////// setters
			
		};

		function getCurrUserData(token){
			$log.debug('[API - getCurrUserData] token',token);
			return $http.get($rootScope.api + '/api/me',{headers:{token:'token '+token}});
		}


	}


}());