var utils = require('../Utils/');

module.exports = {
	pagePost: function(req, res){
		var pages = new Pages({
			name: req.body.name,
			order: req.body.order,
			type: req.body.type,
			copy: req.body.copy,
			state: req.body.state,
			heroPhoto: req.body.mainPhoto
		})
		pages.save(function(err,result){
			if(err) { res.status(500).json(err); }else{
				Pages.findById(pages, function(err,doc){
					if(err) { res.status(500).json(err); }else{
						
						//console.log("restaurant created successfuly uid",doc.uid);
						utils.globalHeaders(res);
						res.json(doc);
					}
				});
			}
		});

	},
	pageGet: function(req, res){
		console.log("****** restaurantGet");
		console.log("req.params.id",req.params.id);
		console.log("req.params.id",req.params.page);

		pages.findOne({'_id': req.params.page}, function(err, page) {
	      if(err) { res.status(500).json(err); }else{
	      	//console.log("restaurant",restaurant);
	      	if(page){
		      	utils.globalHeaders(res);
		      	res.json(page);
		    }else{
		      	res.writeHead(404);
		    	res.end();
		    }
	      }
	    });
	},
	pageUpdate: function(req, res){
		console.log("****** restaurantGet");
		console.log("req.params.id",req.params.id);
		console.log("req.params.id",req.params.page);

		var newpage = {}
		if(req.body.name) newpage.name = req.body.name;
		if(req.body.order) newpage.order = req.body.order;
		if(req.body.type) newpage.type = req.body.type;
		if(req.body.copy) newpage.copy = req.body.copy;
		if(req.body.mainPhoto) newpage.mainPhoto = req.body.mainPhoto;

		Pages.findOneAndUpdate(
			{'_id': req.params.page}, 
			{ $set: newpage },
			{ 'new': true },
			function(err, page) {
		      if(err) { res.status(500).json(err); }else{
		      	//console.log("restaurant",restaurant);
		      	if(page){
			      	utils.globalHeaders(res);
			      	res.json(page);
			    }else{
			      	res.writeHead(404);
			    	res.end();
			    }
		      }
		    }
		);
	},
	pageDelete: function(req, res){
		
	}
}