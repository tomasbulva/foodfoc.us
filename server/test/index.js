var superagent = require('superagent');
var expect = require('expect.js');
var config = require('../config');
var mongoose = require('mongoose'); //(config.MONGO_URI) //, {noClear: true}
var id;
var menuSlug;
var newMenuSlug;
var catId;
var catId2;
var port = 3000; // 3000

describe('RESTAURANT', function(){
  before(function(done) {
    var DatabaseCleaner = require('database-cleaner'); 
    var databaseCleaner = new DatabaseCleaner('mongodb');

    var connect = require('mongodb').connect;

    connect(config.MONGO_URI, function(err, db) {
      databaseCleaner.clean(db, function() {
        console.log('   -> DB CLEAN');
        db.close();
        done();
      });
    });
  });

  it('Create Restaurant', function(done){
    superagent.post('http://foodfoc.us.dev:'+port+'/api/restaurant')
      .send({ 
          name: 'TEST restaurant',
          theme: 'default',
          missionStatement: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',
          scheduleDay: [
              ['08:30','12:00','13:30','16:00'],
              ['08:30','12:00','13:30','16:00'],
              ['08:30','12:00','13:30','16:00'],
              ['08:30','12:00','13:30','16:00'],
              ['08:30','12:00','13:30','16:00'],
              ['08:30','12:30'],
              ['08:30','12:30']
            ],
          phone: '6308418042',
          street: '4145 Via Marina',
          street2: 'apt #302',
          city: 'Marina Del Rey',
          state: 'CA',
          zip: '90292',
          facebookLink: 'https://www.facebook.com/Mylab-203082183068388'
      })
      .end(function(e,res){
        expect(e).to.eql(null);
        expect(typeof res.body).to.eql('object');
        expect(res.body._id.length).to.eql(24);     
        id = res.body.uid;
        done();
      });  
  });

  it('Retrive Restaurant', function(done){
    superagent.get('http://foodfoc.us.dev:'+port+'/api/restaurant/'+id)
      .end(function(e, res){
        // console.log(res.body)
        expect(e).to.eql(null);
        expect(typeof res.body).to.eql('object');
        expect(res.body._id.length).to.eql(24);     
        expect(res.body.uid).to.eql(id);
        done();
      });
  });

  it('Retrive NON-Existent Restaurant', function(done){
    superagent.get('http://foodfoc.us.dev:'+port+'/api/restaurant/foobar')
      .end(function(e, res){
        // console.log(res.body)
        expect(e).to.eql(null);
        expect(res.status).to.equal(404);
        done();
      });
  });

  it('Update Restaurant', function(done){
    superagent.put('http://foodfoc.us.dev:'+port+'/api/restaurant/'+id)
      .send({ 
          theme: 'theme 1',
          missionStatement: 'Lorem Ipsum',
          scheduleDay: [
              ['08:31','12:01','13:31','16:01'],
              ['08:31','12:01','13:31','16:01'],
              ['08:31','12:01','13:31','16:01'],
              ['08:31','12:01','13:31','16:01'],
              ['08:31','12:01','13:31','16:01'],
              ['08:31','12:31'],
              ['08:31','12:31']
            ],
          phone: '6303628867',
          street: '8110 Manitoba street',
          street2: 'apt #216',
          city: 'Playa Del Rey',
          zip: '90293',
      })
      .end(function(e,res){
        expect(e).to.eql(null);
        expect(typeof res.body).to.eql('object');
        expect(res.body._id.length).to.eql(24);     
        id = res.body.uid;
        done();
      });
  });

  it('check on Restaurant update', function(done){
    superagent.get('http://foodfoc.us.dev:'+port+'/api/restaurant/'+id)
      .end(function(e, res){
        //console.log(res.body)
        expect(typeof res.body).to.eql('object');
        expect(res.body._id.length).to.eql(24);     
        expect(res.body.uid).to.eql(id);
        expect(res.body.zip).to.eql('90293');
        done();
      });
  });
  
  it('Delete Restaurant', function(done){
    superagent.del('http://foodfoc.us.dev:'+port+'/api/restaurant/'+id)
      .end(function(e, res){
        //console.log(res.body)
        expect(e).to.eql(null);
        expect(typeof res.body).to.eql('object');
        expect(res.body.ok).to.eql(1);
        done();
      });
  });
});


describe('RESTAURANT-MENU', function(){
  before(function(done) {
    var DatabaseCleaner = require('database-cleaner'); 
    var databaseCleaner = new DatabaseCleaner('mongodb');

    var connect = require('mongodb').connect;

    connect(config.MONGO_URI, function(err, db) {
      databaseCleaner.clean(db, function() {
        console.log('  -> DB CLEAN');
        db.close();
        done();
      });
    });
  });
  it('Create Restaurant', function(done){
    superagent.post('http://foodfoc.us.dev:'+port+'/api/restaurant')
      .send({ 
          name: 'TEST restaurant',
          theme: 'default',
          missionStatement: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',
          scheduleDay: [
              ['08:30','12:00','13:30','16:00'],
              ['08:30','12:00','13:30','16:00'],
              ['08:30','12:00','13:30','16:00'],
              ['08:30','12:00','13:30','16:00'],
              ['08:30','12:00','13:30','16:00'],
              ['08:30','12:30'],
              ['08:30','12:30']
            ],
          phone: '6308418042',
          street: '4145 Via Marina',
          street2: 'apt #302',
          city: 'Marina Del Rey',
          state: 'CA',
          zip: '90292',
          facebookLink: 'https://www.facebook.com/Mylab-203082183068388'
      })
      .end(function(e,res){
        expect(e).to.eql(null);
        expect(typeof res.body).to.eql('object');
        expect(res.body._id.length).to.eql(24);     
        id = res.body.uid;
        done();
      }); 
  });

  it('Create RestaurantMenu', function(done){
    superagent.post('http://foodfoc.us.dev:'+port+'/api/restaurant/'+id+'/menu')
      .send({ 
          name: 'Dinner Menu',
          intro: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.'
      })
      .end(function(e,res){
        expect(e).to.eql(null);
        //console.log("res.body",res.body);
        expect(typeof res.body).to.eql('object');
        expect(res.body._id.length).to.eql(24);
        menuSlug = res.body.slug;
        done();
      });
  });

  it('Retrive RestaurantMenu', function(done){
    superagent.get('http://foodfoc.us.dev:'+port+'/api/restaurant/'+id+'/menu/'+menuSlug)
      .end(function(e, res){
        // console.log(res.body)
        expect(e).to.eql(null);
        expect(typeof res.body).to.eql('object');
        expect(res.body._id.length).to.eql(24);     
        expect(res.body.slug).to.eql('dinner-menu');
        done();
      });
  });

  it('Retrive NON-Existent RestaurantMenu', function(done){
    superagent.get('http://foodfoc.us.dev:'+port+'/api/restaurant/'+id+'/menu/foobar')
      .end(function(e, res){
        // console.log(res.body)
        expect(e).to.eql(null);
        expect(res.status).to.equal(404);
        done();
      });
  });

  it('Create RestaurantMenu 2', function(done){
    superagent.post('http://foodfoc.us.dev:'+port+'/api/restaurant/'+id+'/menu')
      .send({ 
          name: 'Dinner Menu',
          intro: 'Lorem Ipsum hfyfhgk hjkgkg.'
      })
      .end(function(e,res){
        expect(e).to.eql(null);
        //console.log("res.body",res.body);
        expect(typeof res.body).to.eql('object');
        expect(res.body._id.length).to.eql(24);
        menuSlug = res.body.slug;
        done();
      });
  });

  it('Retrive RestaurantMenu with numbered slug', function(done){
    superagent.get('http://foodfoc.us.dev:'+port+'/api/restaurant/'+id+'/menu/'+menuSlug)
      .end(function(e, res){
        // console.log(res.body)
        expect(e).to.eql(null);
        expect(typeof res.body).to.eql('object');
        expect(res.body._id.length).to.eql(24);     
        expect(res.body.slug).to.eql('dinner-menu_1');
        done();
      });
  });

  it('Update RestaurantMenu', function(done){
    superagent.put('http://foodfoc.us.dev:'+port+'/api/restaurant/'+id+'/menu/'+menuSlug)
      .send({ 
          name: 'Lunch Menu',
          intro: 'Lorem Ipsum hfyfhgk 222222.'
      })
      .end(function(e,res){
        expect(e).to.eql(null);
        expect(typeof res.body).to.eql('object');
        expect(res.body._id.length).to.eql(24);
        expect(res.body.name).to.eql('Lunch Menu');
        expect(res.body.slug).to.eql('lunch-menu');
        newMenuSlug = 'lunch-menu';
        done();
      });
  });

  it('Retrive Restaurant with 2 menus', function(done){
    superagent.get('http://foodfoc.us.dev:'+port+'/api/restaurant/'+id)
      .end(function(e, res){
        //console.log(res.body)
        expect(e).to.eql(null);
        expect(typeof res.body).to.eql('object');
        expect(res.body._id.length).to.eql(24);     
        expect(res.body.restaurantMenus.length).to.eql(2);
        done();
      });
  });

  it('Delete RestaurantMenu', function(done){
    superagent.del('http://foodfoc.us.dev:'+port+'/api/restaurant/'+id+'/menu/'+newMenuSlug)
      .end(function(e, res){
        //console.log(res.body);
        expect(e).to.eql(null);
        expect(typeof res.body).to.eql('object');
        expect(res.body.ok).to.eql(1);
        done();
      });
  });

  it('Retrive Restaurant with 1 menu', function(done){
    superagent.get('http://foodfoc.us.dev:'+port+'/api/restaurant/'+id)
      .end(function(e, res){
        //console.log(res.body)
        expect(e).to.eql(null);
        expect(typeof res.body).to.eql('object');
        expect(res.body._id.length).to.eql(24);     
        expect(res.body.restaurantMenus.length).to.eql(1);
        done();
      });
  });

});

describe('RESTAURANT-MENU-CATEGORIE', function(){
  before(function(done) {
    var DatabaseCleaner = require('database-cleaner'); 
    var databaseCleaner = new DatabaseCleaner('mongodb');

    var connect = require('mongodb').connect;

    connect(config.MONGO_URI, function(err, db) {
      databaseCleaner.clean(db, function() {
        console.log('  -> DB CLEAN');
        db.close();
        done();
      });
    });
  });
  it('Create Restaurant', function(done){
    superagent.post('http://foodfoc.us.dev:'+port+'/api/restaurant')
      .send({ 
          name: 'TEST restaurant',
          theme: 'default',
          missionStatement: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',
          scheduleDay: [
              ['08:30','12:00','13:30','16:00'],
              ['08:30','12:00','13:30','16:00'],
              ['08:30','12:00','13:30','16:00'],
              ['08:30','12:00','13:30','16:00'],
              ['08:30','12:00','13:30','16:00'],
              ['08:30','12:30'],
              ['08:30','12:30']
            ],
          phone: '6308418042',
          street: '4145 Via Marina',
          street2: 'apt #302',
          city: 'Marina Del Rey',
          state: 'CA',
          zip: '90292',
          facebookLink: 'https://www.facebook.com/Mylab-203082183068388'
      })
      .end(function(e,res){
        expect(e).to.eql(null);
        expect(typeof res.body).to.eql('object');
        expect(res.body._id.length).to.eql(24);     
        id = res.body.uid;
        done();
      }); 
  });

  it('Create RestaurantMenu', function(done){
    superagent.post('http://foodfoc.us.dev:'+port+'/api/restaurant/'+id+'/menu')
      .send({ 
          name: 'Dinner Menu',
          intro: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.'
      })
      .end(function(e,res){
        expect(e).to.eql(null);
        expect(typeof res.body).to.eql('object');
        expect(res.body._id.length).to.eql(24);
        menuSlug = res.body.slug;
        done();
      });
  });

  it('Create RestaurantMenuCat', function(done){
    superagent.post('http://foodfoc.us.dev:'+port+'/api/restaurant/'+id+'/menu/'+menuSlug+'/category')
      .send({ 
          name: 'Sandwiches',
          description: 'Text of the printing and typesetting industry.',
          order: 0
      })
      .end(function(e,res){
        expect(e).to.eql(null);
        expect(typeof res.body).to.eql('object');
        expect(res.body._id.length).to.eql(24);
        expect(res.body.name).to.eql('Sandwiches');
        //menuSlug = res.body.slug;
        //console.log("menuSlug",menuSlug);
        catId = res.body._id;
        done();
      });
  });

  it('Create RestaurantMenuCat 2', function(done){
    superagent.post('http://foodfoc.us.dev:'+port+'/api/restaurant/'+id+'/menu/'+menuSlug+'/category')
      .send({ 
          name: 'Italian Sandwiches',
          description: 'Text of the sadfsdf and sdfasfsadf industry.',
          order: 0
      })
      .end(function(e,res){
        expect(e).to.eql(null);
        //console.log("res.body",res.body);
        expect(typeof res.body).to.eql('object');
        expect(res.body._id.length).to.eql(24);
        expect(res.body.name).to.eql('Italian Sandwiches');
        //menuSlug = res.body.slug;
        catId2 = res.body._id;
        done();
      });
  });

  it('Retrive RestaurantMenuCat', function(done){
    superagent.get('http://foodfoc.us.dev:'+port+'/api/restaurant/'+id+'/menu/'+menuSlug+'/category/'+catId)
      .end(function(e, res){
        //console.log(res.body)
        expect(e).to.eql(null);
        expect(typeof res.body).to.eql('object');
        expect(res.body._id.length).to.eql(24);     
        expect(res.body.name).to.eql('Sandwiches');
        done();
      });
  });

  it('Retrive NON-Existent RestaurantMenuCat', function(done){
    superagent.get('http://foodfoc.us.dev:'+port+'/api/restaurant/'+id+'/menu/'+menuSlug+'/category/foobar')
      .end(function(e, res){
        //console.log(res.body)
        expect(e).to.eql(null);
        expect(res.status).to.equal(404);
        done();
      });
  });

  it('Update RestaurantMenuCat', function(done){
    superagent.put('http://foodfoc.us.dev:'+port+'/api/restaurant/'+id+'/menu/'+menuSlug+'/category/'+catId)
      .send({ 
          name: 'Hot Sandwiches',
          description: 'Text of the printing and typesetting industry.',
          order: 1
      })
      .end(function(e,res){
        expect(e).to.eql(null);
        expect(typeof res.body).to.eql('object');
        expect(res.body._id.length).to.eql(24);
        expect(res.body.name).to.eql('Hot Sandwiches');
        expect(res.body.order).to.eql(1);
        done();
      });
  });

  it('Retrive RestaurantMenu with 2 categories', function(done){
    superagent.get('http://foodfoc.us.dev:'+port+'/api/restaurant/'+id+'/menu/'+menuSlug)
      .end(function(e, res){
        // console.log(res.body)
        expect(e).to.eql(null);
        expect(typeof res.body).to.eql('object');
        expect(res.body._id.length).to.eql(24);     
        expect(res.body.menuCats.length).to.eql(2);
        done();
      });
  });

  it('Delete RestaurantMenuCat', function(done){
    superagent.del('http://foodfoc.us.dev:'+port+'/api/restaurant/'+id+'/menu/'+menuSlug+'/category/'+catId)
      .end(function(e, res){
        //console.log(res.body);
        expect(e).to.eql(null);
        expect(typeof res.body).to.eql('object');
        expect(res.body.ok).to.eql(1);
        done();
      });
  });

  it('Retrive RestaurantMenu with 1 category', function(done){
    superagent.get('http://foodfoc.us.dev:'+port+'/api/restaurant/'+id+'/menu/'+menuSlug)
      .end(function(e, res){
        // console.log(res.body)
        expect(e).to.eql(null);
        expect(typeof res.body).to.eql('object');
        expect(res.body._id.length).to.eql(24);     
        expect(res.body.menuCats.length).to.eql(1);
        done();
      });
  });

});

describe('RESTAURANT-MENU-CATEGORIE-ITEM', function(){
  before(function(done) {
    var DatabaseCleaner = require('database-cleaner'); 
    var databaseCleaner = new DatabaseCleaner('mongodb');

    var connect = require('mongodb').connect;

    connect(config.MONGO_URI, function(err, db) {
      databaseCleaner.clean(db, function() {
        console.log('  -> DB CLEAN');
        db.close();
        done();
      });
    });
  });
  it('Create Restaurant', function(done){
    superagent.post('http://foodfoc.us.dev:'+port+'/api/restaurant')
      .send({ 
          name: 'TEST restaurant',
          theme: 'default',
          missionStatement: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',
          scheduleDay: [
              ['08:30','12:00','13:30','16:00'],
              ['08:30','12:00','13:30','16:00'],
              ['08:30','12:00','13:30','16:00'],
              ['08:30','12:00','13:30','16:00'],
              ['08:30','12:00','13:30','16:00'],
              ['08:30','12:30'],
              ['08:30','12:30']
            ],
          phone: '6308418042',
          street: '4145 Via Marina',
          street2: 'apt #302',
          city: 'Marina Del Rey',
          state: 'CA',
          zip: '90292',
          facebookLink: 'https://www.facebook.com/Mylab-203082183068388'
      })
      .end(function(e,res){
        expect(e).to.eql(null);
        expect(typeof res.body).to.eql('object');
        expect(res.body._id.length).to.eql(24);     
        id = res.body.uid;
        done();
      }); 
  });

  it('Create RestaurantMenu', function(done){
    superagent.post('http://foodfoc.us.dev:'+port+'/api/restaurant/'+id+'/menu')
      .send({ 
          name: 'Dinner Menu',
          intro: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.'
      })
      .end(function(e,res){
        expect(e).to.eql(null);
        expect(typeof res.body).to.eql('object');
        expect(res.body._id.length).to.eql(24);
        menuSlug = res.body.slug;
        done();
      });
  });

  it('Create RestaurantMenuCat', function(done){
    superagent.post('http://foodfoc.us.dev:'+port+'/api/restaurant/'+id+'/menu/'+menuSlug+'/category')
      .send({ 
          name: 'Sandwiches',
          description: 'Text of the printing and typesetting industry.',
          order: 0
      })
      .end(function(e,res){
        expect(e).to.eql(null);
        //console.log("res.body",res.body);
        expect(typeof res.body).to.eql('object');
        expect(res.body._id.length).to.eql(24);
        expect(res.body.name).to.eql('Sandwiches');
        //menuSlug = res.body.slug;
        catId = res.body._id;
        done();
      });
  });

  it('Create RestaurantMenuCat', function(done){
    superagent.post('http://foodfoc.us.dev:'+port+'/api/restaurant/'+id+'/menu/'+menuSlug+'/category')
      .send({ 
          name: 'Hot Sandwiches',
          description: 'Text of the printing and typesetting industry.',
          order: 0
      })
      .end(function(e,res){
        expect(e).to.eql(null);
        //console.log("res.body",res.body);
        expect(typeof res.body).to.eql('object');
        expect(res.body._id.length).to.eql(24);
        expect(res.body.name).to.eql('Hot Sandwiches');
        //menuSlug = res.body.slug;
        catId2 = res.body._id;
        done();
      });
  });

  it('Create RestaurantMenuItem', function(done){
    superagent.post('http://foodfoc.us.dev:'+port+'/api/restaurant/'+id+'/menu/'+menuSlug+'/category/'+catId+'/item')
      .send({ 
          name: 'Classic Ruben',
          description: 'Classic reuben sandwich with corned beef, dark rye bread, Swiss cheese, sauerkraut, with Russian dressing - grilled.',
          order: 0,
          price: 10.90,
          cat: catId
      })
      .end(function(e,res){
        expect(e).to.eql(null);
        //console.log("res.body",res.body);
        expect(typeof res.body).to.eql('object');
        expect(res.body._id.length).to.eql(24);
        expect(res.body.name).to.eql('Classic Ruben');
        //menuSlug = res.body.slug;
        itemId = res.body._id;
        done();
      });
  });

  it('Retrive RestaurantMenuItem', function(done){
    superagent.get('http://foodfoc.us.dev:'+port+'/api/restaurant/'+id+'/menu/'+menuSlug+'/category/'+catId+'/item/'+itemId)
      .end(function(e, res){
        //console.log("res.body",res.body);
        expect(e).to.eql(null);
        expect(typeof res.body).to.eql('object');
        expect(res.body._id.length).to.eql(24);     
        expect(res.body.name).to.eql('Classic Ruben');
        done();
      });
  });

  it('Retrive NON-Existent RestaurantMenuItem', function(done){
    superagent.get('http://foodfoc.us.dev:'+port+'/api/restaurant/'+id+'/menu/'+menuSlug+'/category/'+catId+'/item/foobar')
      .end(function(e, res){
        // console.log(res.body)
        expect(e).to.eql(null);
        expect(res.status).to.equal(404);
        done();
      });
  });

  it('Update RestaurantMenuItem', function(done){
    superagent.put('http://foodfoc.us.dev:'+port+'/api/restaurant/'+id+'/menu/'+menuSlug+'/category/'+catId+'/item/'+itemId)
      .send({ 
          order: 2,
          price: 13.90,
          cat: catId2
      })
      .end(function(e,res){
        expect(e).to.eql(null);
        console.log(res.body);
        expect(typeof res.body).to.eql('object');
        expect(res.body._id.length).to.eql(24);
        expect(res.body.price).to.eql(13.90);
        expect(res.body.name).to.eql('Classic Ruben');
        expect(res.body.order).to.eql(2);
        done();
      });
  });

  it('Delete RestaurantMenuItem', function(done){
    superagent.del('http://foodfoc.us.dev:'+port+'/api/restaurant/'+id+'/menu/'+menuSlug+'/category/'+catId+'/item/'+itemId)
      .end(function(e, res){
        //console.log(res.body);
        expect(e).to.eql(null);
        expect(typeof res.body).to.eql('object');
        expect(res.body.ok).to.eql(1);
        done();
      });
  });
});


describe('RESTAURANT-PAGE', function(){
  before(function(done) {
    var DatabaseCleaner = require('database-cleaner'); 
    var databaseCleaner = new DatabaseCleaner('mongodb');

    var connect = require('mongodb').connect;

    connect(config.MONGO_URI, function(err, db) {
      databaseCleaner.clean(db, function() {
        console.log('   -> DB CLEAN');
        db.close();
        done();
      });
    });
  });
  it('Create Restaurant', function(done){
    superagent.post('http://foodfoc.us.dev:'+port+'/api/restaurant')
      .send({ 
          name: 'TEST restaurant',
          theme: 'default',
          missionStatement: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',
          scheduleDay: [
              ['08:30','12:00','13:30','16:00'],
              ['08:30','12:00','13:30','16:00'],
              ['08:30','12:00','13:30','16:00'],
              ['08:30','12:00','13:30','16:00'],
              ['08:30','12:00','13:30','16:00'],
              ['08:30','12:30'],
              ['08:30','12:30']
            ],
          phone: '6308418042',
          street: '4145 Via Marina',
          street2: 'apt #302',
          city: 'Marina Del Rey',
          state: 'CA',
          zip: '90292',
          facebookLink: 'https://www.facebook.com/Mylab-203082183068388'
      })
      .end(function(e,res){
        expect(e).to.eql(null);
        expect(typeof res.body).to.eql('object');
        expect(res.body._id.length).to.eql(24);     
        id = res.body.uid;
        done();
      }); 
  });

  it('Create RestaurantPage', function(done){
    superagent.post('http://foodfoc.us.dev:'+port+'/api/restaurant/'+id+'/page')
      .send({ 
          name: 'Dinner Menu',
          intro: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.'
      })
      .end(function(e,res){
        expect(e).to.eql(null);
        expect(typeof res.body).to.eql('object');
        expect(res.body._id.length).to.eql(24);
        menuSlug = res.body.slug;
        done();
      });
  });

  it('Retrive RestaurantPage', function(done){
    superagent.get('http://foodfoc.us.dev:'+port+'/api/restaurant/'+id+'/page/'+pageSlug)
      .end(function(e, res){
        // console.log(res.body)
        expect(e).to.eql(null);
        expect(typeof res.body).to.eql('object');
        expect(res.body._id.length).to.eql(24);     
        expect(res.body.slug).to.eql('dinner-menu');
        done();
      });
  });

  it('Retrive NON-Existent RestaurantMenuCat', function(done){
    superagent.get('http://foodfoc.us.dev:'+port+'/api/restaurant/'+id+'/page/foobarpage')
      .end(function(e, res){
        // console.log(res.body)
        expect(e).to.eql(null);
        expect(res.status).to.equal(404);
        done();
      });
  });

  it('Update RestaurantMenuCat', function(done){
    superagent.put('http://foodfoc.us.dev:'+port+'/api/restaurant/'+id+'/page/'+pageSlug)
      .send({ 
          name: 'Hot Sandwiches',
          description: 'Text of the printing and typesetting industry.',
          order: 1
      })
      .end(function(e,res){
        expect(e).to.eql(null);
        expect(typeof res.body).to.eql('object');
        expect(res.body._id.length).to.eql(24);
        expect(res.body.name).to.eql('Hot Sandwiches');
        expect(res.body.order).to.eql(1);
        done();
      });
  });

  it('Delete RestaurantMenuCat', function(done){
    superagent.del('http://foodfoc.us.dev:'+port+'/api/restaurant/'+id+'/page/'+pageSlug)
      .end(function(e, res){
        //console.log(res.body);
        expect(e).to.eql(null);
        expect(typeof res.body).to.eql('object');
        expect(res.body.ok).to.eql(1);
        done();
      });
  });

});

// describe('RESTAURANT-MEDIA', function(){
//   before(function(done) {
//     var DatabaseCleaner = require('database-cleaner'); 
//     var databaseCleaner = new DatabaseCleaner('mongodb');

//     var connect = require('mongodb').connect;

//     connect(config.MONGO_URI, function(err, db) {
//       databaseCleaner.clean(db, function() {
//         console.log('   -> DB CLEAN');
//         db.close();
//         done();
//       });
//     });
//   });
//   it('Create Restaurant', function(done){
//     superagent.post('http://foodfoc.us.dev:'+port+'/api/restaurant')
//       .send({ 
//           name: 'TEST restaurant',
//           theme: 'default',
//           missionStatement: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',
//           scheduleDay: [
//               ['08:30','12:00','13:30','16:00'],
//               ['08:30','12:00','13:30','16:00'],
//               ['08:30','12:00','13:30','16:00'],
//               ['08:30','12:00','13:30','16:00'],
//               ['08:30','12:00','13:30','16:00'],
//               ['08:30','12:30'],
//               ['08:30','12:30']
//             ],
//           phone: '6308418042',
//           street: '4145 Via Marina',
//           street2: 'apt #302',
//           city: 'Marina Del Rey',
//           state: 'CA',
//           zip: '90292',
//           facebookLink: 'https://www.facebook.com/Mylab-203082183068388'
//       })
//       .end(function(e,res){
//         expect(e).to.eql(null);
//         expect(typeof res.body).to.eql('object');
//         expect(res.body._id.length).to.eql(24);     
//         id = res.body.uid;
//         done();
//       }); 
//   });

//   it('Create RestaurantMedia', function(done){
//     superagent.post('http://foodfoc.us.dev:'+port+'/api/restaurant/'+id+'/'+menuSlug+'/media')
//       .send({ 
//           name: 'Sandwiches',
//           description: 'Text of the printing and typesetting industry.',
//           order: 0
//       })
//       .end(function(e,res){
//         expect(e).to.eql(null);
//         //console.log("res.body",res.body);
//         expect(typeof res.body).to.eql('object');
//         expect(res.body._id.length).to.eql(24);
//         expect(res.body.name).to.eql('Sandwiches');
//         menuSlug = res.body.slug;
//         catId = res.body._id;
//         done();
//       });
//   });

//   it('Retrive RestaurantMenuCat', function(done){
//     superagent.get('http://foodfoc.us.dev:'+port+'/api/restaurant/'+id+'/'+menuSlug+'/'+catId)
//       .end(function(e, res){
//         // console.log(res.body)
//         expect(e).to.eql(null);
//         expect(typeof res.body).to.eql('object');
//         expect(res.body._id.length).to.eql(24);     
//         expect(res.body.slug).to.eql('dinner-menu');
//         done();
//       });
//   });

//   it('Retrive NON-Existent RestaurantMenuCat', function(done){
//     superagent.get('http://foodfoc.us.dev:'+port+'/api/restaurant/'+id+'/'+menuSlug+'/foobar')
//       .end(function(e, res){
//         // console.log(res.body)
//         expect(e).to.eql(null);
//         expect(res.status).to.equal(404);
//         done();
//       });
//   });

//   it('Update RestaurantMenuCat', function(done){
//     superagent.put('http://foodfoc.us.dev:'+port+'/api/restaurant/'+id+'/'+menuSlug+'/'+catId)
//       .send({ 
//           name: 'Hot Sandwiches',
//           description: 'Text of the printing and typesetting industry.',
//           order: 1
//       })
//       .end(function(e,res){
//         expect(e).to.eql(null);
//         expect(typeof res.body).to.eql('object');
//         expect(res.body._id.length).to.eql(24);
//         expect(res.body.name).to.eql('Hot Sandwiches');
//         expect(res.body.order).to.eql(1);
//         done();
//       });
//   });

//   it('Delete RestaurantMenuCat', function(done){
//     superagent.del('http://foodfoc.us.dev:'+port+'/api/restaurant/'+id+'/'+menuSlug+'/'+catId)
//       .end(function(e, res){
//         //console.log(res.body);
//         expect(e).to.eql(null);
//         expect(typeof res.body).to.eql('object');
//         expect(res.body.ok).to.eql(1);
//         done();
//       });
//   });

// });


