#!/usr/bin/env bash

# Use single quotes instead of double quotes to make it work with special-character passwords
# PASSWORD='1234'
# PROJECTFOLDER='SDA-2015'
# DBNAME='sda2015'
# DBFILE='sda2015.sql'

# create project folder
#sudo mkdir "/var/www"
#sudo chown www-data:www-data "/var/www"
#sudo ln -s "/vagrant" "/var/www/${PROJECTFOLDER}"
sudo usermod -a -G www-data vagrant
# update / upgrade
sudo apt-get update
sudo apt-get -y upgrade

# install apache 2.5 and php 5.5
sudo apt-get install -y apache2
sudo apt-get install -y php5
sudo apt-get install -y nodejs
sudo ln -s  /usr/bin/nodejs  /usr/bin/node
sudo apt-get install -y npm

sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 7F0CEB10
echo "deb http://repo.mongodb.org/apt/ubuntu "$(lsb_release -sc)"/mongodb-org/3.0 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.0.list
sudo apt-get update

sudo mkdir /data -m a=rw
sudo mkdir /data/db -m a=rw

sudo apt-get install -y mongodb-org

MDBCONF=$(cat <<EOF 
# mongod.conf

# Where to store the data.

# Note: if you run mongodb as a non-root user (recommended) you may
# need to create and set permissions for this directory manually,
# e.g., if the parent directory isn't mutable by the mongodb user.
dbpath=/var/lib/mongodb

#where to log
logpath=/var/log/mongodb/mongod.log

logappend=true

#port = 27017

# Listen to local interface only. Comment out to listen on all interfaces.
#bind_ip = 127.0.0.1

# Disables write-ahead journaling
# nojournal = true

# Enables periodic logging of CPU utilization and I/O wait
#cpu = true

# Turn on/off security.  Off is currently the default
#noauth = true
#auth = true

# Verbose logging output.
#verbose = true

# Inspect all client data for validity on receipt (useful for
# developing drivers)
#objcheck = true

# Enable db quota management
#quota = true

# Set oplogging level where n is
#   0=off (default)
#   1=W
#   2=R
#   3=both
#   7=W+some reads
#diaglog = 0

# Ignore query hints
#nohints = true

# Enable the HTTP interface (Defaults to port 28017).
#httpinterface = true

# Turns off server-side scripting.  This will result in greatly limited
# functionality
#noscripting = true

# Turns off table scans.  Any query that would do a table scan fails.
#notablescan = true

# Disable data file preallocation.
#noprealloc = true

# Specify .ns file size for new databases.
#nssize = <size>

# Replication Options

# in replicated mongo databases, specify the replica set name here
#replSet=setname
# maximum size in megabytes for replication operation log
#oplogSize=1024
# path to a key file storing authentication info for connections
# between replica set members
#keyFile=/path/to/keyfile
EOF
)
echo "${MDBCONF}" > /etc/mongod.conf
sudo service mongod restart

sudo npm install -g mongo-express
sudo cp /usr/local/lib/node_modules/mongo-express/config.default.js /usr/local/lib/node_modules/mongo-express/config.js

# setup hosts file
VHOST1=$(cat <<EOF
<VirtualHost *:80>
    DocumentRoot "/var/www/server/"
    ServerName foodfoc.us.dev
    <Directory "/var/www/server/">
        AllowOverride All
        Require all granted
    </Directory>
</VirtualHost>
EOF
)
echo "${VHOST1}" > /etc/apache2/sites-available/000-default.conf

# enable mod_rewrite
sudo a2enmod rewrite

# restart apache
service apache2 restart

# install git
sudo apt-get -y install git

npm install -g nodemon

 


