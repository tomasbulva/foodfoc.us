var mongoose = require('mongoose');
var utils = require('../Utils/');

var restaurantSchema = new mongoose.Schema({
  name: { type: String, required: true },
  domain: String,
  uid: { type: String, unique: true },
  theme: String,
  logoImage: mongoose.Schema.Types.ObjectId,
  headerImage: mongoose.Schema.Types.ObjectId,
  missionStatement: String,


  // scheduleDay = [
  //   [08:30,12:00,13:30,16:00],
  //   [08:30,12:00,13:30,16:00],
  //   [08:30,12:00,13:30,16:00],
  //   [08:30,12:00,13:30,16:00],
  //   [08:30,12:00,13:30,16:00],
  //   [08:30,12:30],
  //   [08:30,12:30]
  // ]

  scheduleDay: [[String]],
  phone: String,
  street: String,
  street2: String,
  city: String,
  state: String,
  zip: String,
  facebookLink: String,
  yelpLink: String,
  tripadvisorLink: String,
  twitterLink: String,
  instagramLink: String,
  restaurantMenus: [{type: mongoose.Schema.Types.ObjectId, ref: 'RestaurantMenu'}],
  restaurantPages: [{type: mongoose.Schema.Types.ObjectId, ref: 'RestaurantPages'}],
  users: [],//[mongoose.Schema.Types.ObjectId],
  ga: String,
});

restaurantSchema.pre('save', function(next) {
  var restaurant = this;
  //console.log('restaurant',restaurant.users.length);
  if(restaurant.uid === undefined) {
    restaurant.uid = utils.generateToken(12);
  }
  next();
});

module.exports = mongoose.model('Restaurant', restaurantSchema);

