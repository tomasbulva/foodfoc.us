var Restaurant = require('./RestaurantModel');
var utils = require('../Utils/');

module.exports = {
	restaurantPost: function(req, res){
		//console.log("****** restaurantPost");
		//console.log("req.body",req.body);
		//console.log("req",req);

		var restaurant = new Restaurant({
			name: req.body.name,
			
			// -->  create upload req.body.logoImage: mongoose.Schema.Types.ObjectId,
			// -->  create upload req.body.headerImage: mongoose.Schema.Types.ObjectId,
			
			missionStatement: req.body.missionStatement,
			scheduleDay: req.body.scheduleDay,
			phone: req.body.phone,
			street: req.body.street,
			street2: req.body.street2,
			city: req.body.city,
			state: req.body.state,
			zip: req.body.zip,
			facebookLink: req.body.facebookLink,
			yelpLink: req.body.yelpLink,
			tripadvisorLink: req.body.tripadvisorLink,
			twitterLink: req.body.twitterLink,
			instagramLink: req.body.instagramLink,
			ga: req.body.ga,
			users: ['']//[req.user._id]
			pages: [mongoose.Schema.Types.ObjectId]
		});

		// TODO:
		// the UID is being automatically generated in MODEL file. 
		// I need to add mechanism that will garantee uniqness of UID. 
		// 
		restaurant.save(function(err) {
			//console.log("cb1 err",err);
			if(err) { res.status(500).json(err); }else{
				Restaurant.findById(restaurant, function(err,doc){
					if(err) { res.status(500).json(err); }else{
						console.log("restaurant created successfuly uid",doc.uid);
						utils.globalHeaders(res);
						res.json(doc);
					}
				});
			}
        });
        //console.log("restaurant object 2",restaurant);
	},
	restaurantGet: function(req, res){
		console.log("****** restaurantGet");
		console.log("req.params.id",req.params.id);
		Restaurant.findOne({'uid': req.params.id}, function(err, restaurant) {
	      if(err) { res.status(500).json(err); }else{
	      	//console.log("restaurant",restaurant);
	      	if(restaurant){
		      	utils.globalHeaders(res);
		      	res.json(restaurant);
		    }else{
		      	res.writeHead(404);
		    	res.end();
		    }
	      }
	    });
	},
	restaurantUpdate: function(req,res){
		// console.log("****** restaurantUpdate");
		// console.log("req.params.id",req.params.id);
		// console.log("req.body",req.body);
		Restaurant.findOneAndUpdate({'uid': req.params.id},req.body, function(err, restaurant) {
	      if(err) { res.status(500).json(err); }else{
	      	utils.globalHeaders(res);
	      	res.json(restaurant);
	      }
	    });
	},
	restaurantDelete: function(req,res){
		// console.log("****** restaurantDelete");
		// console.log("req.params.id",req.params.id);
		Restaurant.find({'uid': req.params.id}).remove( function(err, restaurant) {
	      if(err) { res.status(500).json(err); }else{
	      	utils.globalHeaders(res);
	      	res.json(restaurant);
	      }
	    });
	}
};