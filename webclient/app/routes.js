(function() {
    'use strict';

    angular
        .module('app')
        .config(Routes);

    ///////////
    //Routes.$inject = ['$stateProvider', '$urlRouterProvider', '$locationProvider', '$httpProvider','$log'];
    function Routes($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider) {
        //$log.debug("*************** Routes ****************");
        

        $stateProvider
            .state('home', {
                url: '/',
                views: {
                    'header@': {
                        templateUrl: 'app/home/partial-header.html',
                        controller: 'HomeHeaderController'
                    },
                    'body@': {
                        templateUrl: 'app/home/partial-body.html',
                        controller: 'HomeBodyController'
                    }
                }
            })
            .state('home.login', {
                url: 'login',
                resolve: {
                    loginRequired: ['$q', '$auth', function ($q, $auth) {
                      var deferred = $q.defer();
                      if ($auth.isAuthenticated()) {
                        deferred.reject();
                      } else {
                        deferred.resolve();
                      }
                      return deferred.promise;
                    }]
                },
                views: {
                    'header@': {
                        templateUrl: 'app/login/partial-header.html',
                        controller: 'LoginHeaderController'
                    },
                    'body@': {
                        templateUrl: 'app/login/partial-body.html',
                        controller: 'LoginBodyController'
                    }
                }
            })
            .state('home.dashboard', {
                url: 'dashboard',
                resolve: {
                    loginRequired: ['$q', '$location', '$auth', function ($q, $location, $auth) {
                      var deferred = $q.defer();
                      if ($auth.isAuthenticated()) {
                        deferred.resolve();
                      } else {
                        $location.path('/login');
                      }
                      return deferred.promise;
                    }],
                    currUser: ['$q', '$rootScope', 'API', '$auth', '$log', function($q, $rootScope, API, $auth, $log) {
                        console.log("resolve currUser");
                        var deferred = $q.defer();
                        API.getCurrUser().then(function(r) {
                            deferred.resolve(r);
                            $rootScope.currUser = r.data;
                            $log.debug('[dashboard - routes resolve] $rootScope.currUser', $rootScope.currUser);
                            //serverLog.debug({label:'[SdaRoutesFce] state->home getCurrUser', message: r.data});
                        }, function(reason){
                           //serverLog.error({label:'[SdaRoutesFce] state->home getCurrUser', message: reason});
                           //document.location.href = $rootScope.siteurl+"/wp-login.php";
                           $log.error('[dashboard - routes resolve] error reason', reason);
                        });
                        return deferred.promise;
                    }]
                },
                views: {
                    'header@': {
                        templateUrl: 'app/dashboard/partial-header.html',
                        controller: 'DashboardHeaderController'
                    },
                    'body@': {
                        templateUrl: 'app/dashboard/partial-body.html',
                        controller: 'DashboardBodyController'
                    }
                }
            })
        ;
        $urlRouterProvider.otherwise("/");

    }


    ///////////// end closure   <!-- <div ui-view></div> -->
})();
