var Restaurant = require('../Restaurant/RestaurantModel');
var RestaurantMenu = require('./RestaurantMenuModel');
var RestaurantMenuCat = require('./RestaurantMenuCatModel');
var RestaurantMenuItem = require('./RestaurantMenuItemModel');
var utils = require('../Utils/');

module.exports = {
	menuPost: function(req, res){
		console.log("****** menuPost");
		console.log("req.params.id",req.params.id);
		Restaurant.findOne({'uid': req.params.id}, function(err, restaurant) {
			if(err) { 
				res.status(500).json(err);
				return false;
			}

			console.log('restaurant.uid',restaurant.uid);
			if(restaurant.uid === undefined || restaurant.uid === '') {
				console.log('restaurant not-found');
				res.writeHead(404);
				res.end();
				return false;
			}

			var restaurantMenu = new RestaurantMenu({
				name: req.body.name,
	  			intro: req.body.intro,
	  			restaurant: restaurant
			});

			restaurantMenu.save(function(err) {
				//console.log("cb1 err",err);
				if(err) { res.status(500).json(err); }else{
					//console.log("cb2 err",err);
					restaurant.restaurantMenus.push(restaurantMenu);
					//console.log("restaurant.restaurantMenus",restaurant.restaurantMenus);
					restaurant.save(function(err){
						//console.log("cb3 err",err);
						if(err) { res.status(500).json(err); }else{
							RestaurantMenu.findById(restaurantMenu, function(err,doc){
								//console.log("cb4 err",err);
								if(err) { res.status(500).json(err); }else{
									utils.globalHeaders(res);
									res.json(doc);
								}
							});
						}
					});
				}
	        });
		});
	},
	menuGet: function(req, res){
		console.log("****** menuGet");
		console.log("req.params.id",req.params.id);
		console.log('req.params.menu',req.params.menu);
		Restaurant
			.findOne({'uid': req.params.id})
			.populate({
				path:'restaurantMenus',
				match: { slug: req.params.menu}
			})
			.exec(function(err, restaurant) {
		      if(err) { res.status(500).json(err); }else{
		      	//console.log("restaurant.restaurantMenus",restaurant.restaurantMenus);
		      	//console.log("restaurant",restaurant);
		      	if(restaurant && restaurant.restaurantMenus && restaurant.restaurantMenus.length){
		      		utils.globalHeaders(res);
		      		res.json(restaurant.restaurantMenus[0]);
		      	}else{
		      		res.writeHead(404)
		      		res.end();
		      	}
		      }
		    }
		);
	},
	menuUpdate: function(req,res){
		console.log("****** menuUpdate");
		console.log("req.params.id",req.params.id);
		console.log("req.body",req.body);
		Restaurant
			.findOne({'uid': req.params.id})
			.populate({
				path:'restaurantMenus',
				match: { slug: req.params.menu}
			})
			.exec(function(err, restaurant) {
				console.log("1 err",err);
				if(err) { res.status(500).json(err); }else{
			      	console.log("restaurant.restaurantMenus",restaurant.restaurantMenus);
			      	//console.log("restaurant",restaurant);
			      	if(restaurant && restaurant.restaurantMenus && restaurant.restaurantMenus.length){
			      		
			      		var newObj = {};
			      		if(req.body.name) newObj.name = req.body.name;
			      		if(req.body.intro) newObj.intro = req.body.intro;

			      		RestaurantMenu.findOneAndUpdate(
			      			{ '_id':restaurant.restaurantMenus[0]._id },
			      			{ $set: newObj },
							{ 'new': true },
							function(err, result) {
								console.log("2 err",err);
								console.log("result",result);
								if(err) { res.status(500).json(err); }else{
									if(result){
										utils.globalHeaders(res);
										//console.log('result',result);
										res.json(result);
									}else{
							      		res.writeHead(404);
							    		res.end();
							      	}
							    }
						});
			      	}else{
			      		res.writeHead(404);
			    		res.end();
			      	}
			     }
		});
	},
	menuDelete: function(req,res){
		console.log("****** menuDelete");
		console.log("req.params.id",req.params.id);
		Restaurant
			.findOne({'uid': req.params.id})
			.populate({
				path:'restaurantMenus',
				match: { slug: req.params.menu}
			})
			.exec(function(err, restaurant) {
				//console.log("1 err",err);
				if(err) { res.status(500).json(err); }else{
			      	//console.log("restaurant.restaurantMenus",restaurant.restaurantMenus);
			      	//console.log("restaurant",restaurant);
			      	if(restaurant && restaurant.restaurantMenus && restaurant.restaurantMenus.length){
			      		Restaurant.update({
						    restaurantMenus: restaurant.restaurantMenus[0]._id
						}, {
						    '$pull': {
						        restaurantMenus: restaurant.restaurantMenus[0]._id
						    }
						},
						function(err,doc){
							if(err) { res.status(500).json(err); }else{
								if(doc){
									RestaurantMenu.find({'_id':restaurant.restaurantMenus[0]._id}).remove(function(err, result) {
										//console.log("2 err",err);
										//console.log("result",result);
										if(err) { res.status(500).json(err); }else{
											if(result){
												utils.globalHeaders(res);
												//console.log('result',result);
												res.json(result);
											}else{
									      		res.writeHead(404);
									    		res.end();
									    		return false;
									      	}
									    }
									});
								}else{
						      		res.writeHead(404);
						    		res.end();
						      	}
						    }
						});
			      	}else{
			      		res.writeHead(404);
			    		res.end();
			      	}
			    }
		});
	},

	catPost: function(req,res){
		console.log("===================================");
		console.log("****** catPost");
		console.log("req.params.id",req.params.id);
		console.log("req.params.menu",req.params.menu);

		Restaurant
			.findOne({'uid': req.params.id})
			.populate({
				path:'restaurantMenus',
				match: { slug: req.params.menu}
			})
			.exec(function(err, restaurant) {
				//console.log("1 err",err);
				if(err) { res.status(500).json(err); }else{
					var restaurantMenuCat = new RestaurantMenuCat({
						name: req.body.name,
			  			description: req.body.description,
			  			order: req.body.order,
			  			menu: restaurant.restaurantMenus[0],
			  			items: []
					});

					restaurantMenuCat.save(function(err, result){
						console.log("[RestaurantMenu.findOneAndUpdate] restaurantMenuCat.save result",result);
						if(err) { res.status(500).json(err); }else{
							RestaurantMenuCat.findById(restaurantMenuCat, function(err,doc){
								console.log("[RestaurantMenu.findOneAndUpdate] RestaurantMenuCat.findById doc",doc);
								console.log("[RestaurantMenu.findOneAndUpdate] RestaurantMenuCat.findById err",err);
								console.log("[RestaurantMenu.findOneAndUpdate] restaurant.restaurantMenus[0]",restaurant.restaurantMenus[0]);

								if(err) { res.status(500).json(err); }else{
									RestaurantMenu.findOneAndUpdate(
										{'_id':restaurant.restaurantMenus[0]._id}, 
										{$push: {'menuCats': restaurantMenuCat}},
										{ safe: true, upsert: true, new: true },
										function(err, result) {
											console.log("[RestaurantMenu.findOneAndUpdate] err",err);
											console.log("[RestaurantMenu.findOneAndUpdate] result",doc);
											if(err) { res.status(500).json(err); }else{
												if(result){
													utils.globalHeaders(res);
													res.json(doc);
												}else{
										      		res.writeHead(404);
										    		res.end();
										      	}
										    }
									});
								}
							});
						}
					});
				}
		});
	},
	catGet: function(req,res){
		console.log("****** catGet");
		console.log("req.params.id",req.params.id);
		console.log("req.params.menu",req.params.menu);
		console.log("req.params.category",req.params.category);

		if(!utils.checkValidDbId(req.params.category)){
			res.writeHead(404);
		    res.end();
		    return false;
		}

		RestaurantMenuCat.findById(req.params.category, function(err,doc){
			if(err) { res.status(500).json(err); }else{
				if(doc){
					utils.globalHeaders(res);
					res.json(doc);
				}else{
		      		res.writeHead(404);
		    		res.end();
		      	}
		    }
		});
	},
	catUpdate: function(req,res){
		console.log("****** catUpdate");
		console.log("req.params.id",req.params.id);
		console.log("req.params.menu",req.params.menu);
		console.log("req.params.category",req.params.category);

		if(!utils.checkValidDbId(req.params.category)){
			res.writeHead(404);
		    res.end();
		}

		var newObj = {}
		if(req.body.name) newObj.name = req.body.name;
		if(req.body.description) newObj.description = req.body.description;
		if(req.body.order) newObj.order = req.body.order;
		if(req.body.menu) newObj.menu = req.body.menu;
		if(req.body.items) newObj.items = req.body.items;

		RestaurantMenuCat.findOneAndUpdate(
			{'_id':req.params.category}, 
			{$set: newObj},
  			{ 'new': true }, 
  			function(err, result) {
				console.log("2 err",err);
				console.log("result",result);
				if(err) { res.status(500).json(err); }else{
					if(result){
						utils.globalHeaders(res);
						//console.log('result',result);
						res.json(result);
					}else{
			      		res.writeHead(404);
			    		res.end();
			      	}
			    }
			}
		);
	},
	catDelete: function(req,res){
		console.log("****** catDelete");
		console.log("req.params.id",req.params.id);
		console.log("req.params.menu",req.params.menu);
		console.log("req.params.category",req.params.category);

		Restaurant
			.findOne({'uid': req.params.id})
			.populate({
				path:'restaurantMenus',
				match: { slug: req.params.menu}
			})
			.exec(function(err, restaurant) {
				//console.log("1 err",err);
				if(err) { res.status(500).json(err); }else{
			      	//console.log("restaurant.restaurantMenus",restaurant.restaurantMenus);
			      	//console.log("restaurant",restaurant);
			      	if(restaurant && restaurant.restaurantMenus && restaurant.restaurantMenus.length){
			      		RestaurantMenu.update({
						    menuCats: req.params.category
						}, {
						    '$pull': {
						        menuCats: req.params.category
						    }
						},
						function(err,doc){
							if(err) { res.status(500).json(err); }else{
								if(doc){
									RestaurantMenuCat.find({'_id':req.params.category}).remove(function(err, result) {
										//console.log("2 err",err);
										//console.log("result",result);
										if(err) { res.status(500).json(err); }else{
											if(result){
												utils.globalHeaders(res);
												//console.log('result',result);
												res.json(result);
											}else{
									      		res.writeHead(404);
									    		res.end();
									    		return false;
									      	}
									    }
									});
								}else{
						      		res.writeHead(404);
						    		res.end();
						      	}
						    }
						});
			      	}else{
			      		res.writeHead(404);
			    		res.end();
			      	}
			    }
		});	
	},

	itemPost: function(req,res){
		console.log("===================================");
		console.log("****** itemPost");
		console.log("req.params.id",req.params.id);
		console.log("req.params.menu",req.params.menu);
		console.log("req.params.category",req.params.category);
		//console.log("req.params.item",req.params.item);
		var restaurantMenuItem = new RestaurantMenuItem({
			name: req.body.name,
			description: req.body.description,
			order: (req.body.order) ? req.body.order : 0,
			price: req.body.price,
			photo: (req.body.photo) ? req.body.photo : [],
			cat: req.params.category
		});
		restaurantMenuItem.save(function(err, result){
			console.log("cb1 err",err);
			console.log("cb1 result",result);
			if(err) { res.status(500).json(err); }else{
				//console.log("cb2 err",err);
				RestaurantMenuCat.findOneAndUpdate(
					{"_id":req.params.category},
					{$push: {"items":result._id}},
					{ safe: true, upsert: true, new: true },
					function(err, doc){
						console.log("cb2 err",err);
						if(err) { res.status(500).json(err); }else{
							utils.globalHeaders(res);
							res.json(result);
						}
					}
				);
			}
		});
	},
	itemGet: function(req,res){
		console.log("===================================");
		console.log("****** itemGet");
		console.log("req.params.id",req.params.id);
		console.log("req.params.menu",req.params.menu);
		console.log("req.params.category",req.params.category);
		console.log("req.params.item",req.params.item);

		if(!utils.checkValidDbId(req.params.category) || !utils.checkValidDbId(req.params.item)){
			res.writeHead(404);
		    res.end();
		    return false;
		}

		RestaurantMenuItem.findOne({"_id":req.params.item},function(err,result){
			console.log("cb1 err",err);
			console.log("cb1 result",result);
			if(err) { res.status(500).json(err); }else{
				utils.globalHeaders(res);
				res.json(result);
			}
		});
	},
	itemUpdate: function(req,res){
		console.log("===================================");
		console.log("****** itemUpdate");
		console.log("req.params.id",req.params.id);
		console.log("req.params.menu",req.params.menu);
		console.log("req.params.category",req.params.category);
		console.log("req.params.item",req.params.item);

		if(!utils.checkValidDbId(req.params.item)){
			res.writeHead(404);
		    res.end();
		    return false;
		}

		var newObj = {};
		if (req.body.name) newObj.name = req.body.name;
		if (req.body.description) newObj.description = req.body.description;
		if (req.body.order) newObj.order = req.body.order;
		if (req.body.price) newObj.price = req.body.price;
		if (req.body.photo) newObj.photo = req.body.photo;
		if (req.params.category) newObj.cat = req.params.category;

		RestaurantMenuItem.findOneAndUpdate(
			{"_id":req.params.item},
			{$set: newObj},
			{ safe: true, upsert: true, new: true },
			function(err,result){
			console.log("cb1 err",err);
			console.log("cb1 result",result);
			if(err) { res.status(500).json(err); }else{
				utils.globalHeaders(res);
				res.json(result);
			}
		});
	},
	itemDelete: function(req,res){
		console.log("===================================");
		console.log("****** itemDelete");
		console.log("req.params.id",req.params.id);
		console.log("req.params.menu",req.params.menu);
		console.log("req.params.category",req.params.category);
		console.log("req.params.item",req.params.item);

		if(!utils.checkValidDbId(req.params.category) || !utils.checkValidDbId(req.params.item)){
			res.writeHead(404);
		    res.end();
		    return false;
		}

		RestaurantMenuCat.update(
			{ menuCats: req.params.category }, 
			{ '$pull': { menuCats: req.params.category }},
			function(err,doc){
				if(err) { res.status(500).json(err); }else{
					if(doc){
						RestaurantMenuCat.findOne({'_id':req.params.item}).remove(function(err, result) {
							//console.log("2 err",err);
							//console.log("result",result);
							if(err) { res.status(500).json(err); }else{
								if(result){
									utils.globalHeaders(res);
									//console.log('result',result);
									res.json(result);
								}else{
						      		res.writeHead(404);
						    		res.end();
						    		return false;
						      	}
						    }
						});
					}else{
						res.writeHead(404);
						res.end();
					}
				}
			}
		);
	}

};
