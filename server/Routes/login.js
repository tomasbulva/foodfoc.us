var authController = require('../Authentication/');

var express 	= require("express");
var app 		= module.exports = express();

app.post('/login', authController.authLogin);
app.post('/signup', authController.authSignup);
app.post('/google', authController.authGoogle);
app.post('/github', authController.authGithub);
app.post('/instagram', authController.authInstagram);
app.post('/linkedin', authController.authLinkedin);
app.post('/live', authController.authLive);
app.post('/facebook', authController.authFacebook);
app.post('/yahoo', authController.authYahoo);
app.post('/twitter', authController.authTwitter);
app.post('/foursquare', authController.authFoursquare);
app.post('/twitch', authController.authTwitch);
