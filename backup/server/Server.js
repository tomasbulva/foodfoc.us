var http = require('http');
var express = require('express');
var path = require('path');
var fs = require('fs');
var logger = require('morgan');
var methodOverride = require('method-override');
var session = require('express-session');
var bodyParser = require('body-parser');
var multer = require('multer');
var errorHandler = require('errorhandler');
var cors = require('cors');

var winston = require('winston');
var mongoose = require('mongoose');

var config = require('./config');

var app = express();

// all environments
app.set('port', process.env.PORT || 3000);

mongoose.connect(config.MONGO_URI);
mongoose.connection.on('error', function(err) {
  console.error('Error: Could not connect to MongoDB. Did you forget to run `mongod`?'.red);
});

//app.use(cors());
app.use(logger('dev'));
app.use(methodOverride());
app.use(session({ resave: true,
                  saveUninitialized: true,
                  secret: 'uwotm8' }));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(multer({dest:'./uploads/'}).single('photo'));
app.use(express.static(path.join(__dirname, 'public')));

// ## CORS middleware
// 
// see: http://stackoverflow.com/questions/7067966/how-to-allow-cors-in-express-nodejs
var allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization');
      
    // intercept OPTIONS method
    if ('OPTIONS' == req.method) {
      res.send(200);
    }
    else {
      next();
    }
};
app.use(allowCrossDomain);

var loginRoute = require('./Routes/login');
var apiRoutes = require('./Routes/');

app.use('/auth', loginRoute);
app.use('/api', apiRoutes);

// error handling middleware should be loaded after the loading the routes
if ('development' == app.get('env')) {
  app.use(errorHandler());
}

var server = http.createServer(app);
server.listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});