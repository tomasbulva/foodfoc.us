var mongoose = require('mongoose');

var restaurantMenuItemSchema = new mongoose.Schema({
  name: String,
  description: String,
  order: String,
  price: String,
  photo: [mongoose.Schema.Types.ObjectId],
  cat: mongoose.Schema.Types.ObjectId
});

module.exports = mongoose.model('RestaurantMenuItem', restaurantMenuItemSchema);
