var path = require('path');
var qs = require('querystring');

var async = require('async');
var bcrypt = require('bcryptjs');
var bodyParser = require('body-parser');
var colors = require('colors');
var cors = require('cors');
var express = require('express');
var logger = require('morgan');
var jwt = require('jwt-simple');
var moment = require('moment');
var User = require('./UserModel');
var mongoose = require('mongoose');
var request = require('request');
var config = require('../config');


// USER model:
// -----------------------------
//    email:             { type: String, unique: true, lowercase: true },
//    password:          { type: String, select: false },
//    displayName:       String,
//    picture:           String,
//    restaurant:        Array,
//    facebook:          String,
//    foursquare:        String,
//    google:            String,
//    github:            String,
//    instagram:         String,
//    linkedin:          String,
//    live:              String,
//    yahoo:             String,
//    twitter:           String,
//    twitch:            String
// ----------------------------


module.exports = {
  /* -----------------------------------------------
                    API Routes
  ----------------------------------------------- */

  /* GET /api/me */
  userMeGet: function(req, res) {
    User.findById(req.user, function(err, user) {
      res.send(user);
    });
  },

  /* PUT /api/me */
  userMePut: function(req, res) {
    User.findById(req.user, function(err, user) {
      if (!user) {
        return res.status(400).send({ message: 'User not found' });
      }
      user.displayName = req.body.displayName || user.displayName;
      user.email = req.body.email || user.email;
      user.save(function(err) {
        res.status(200).end();
      });
    });
  }

  /* -------------------------------------------- */

};