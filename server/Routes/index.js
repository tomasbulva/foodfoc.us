var authController = require('../Authentication/');
var UserController = require('../User/');
var RestaurantController = require('../Restaurant/');
var RestaurantMenuController = require('../RestaurantMenu/');

var express 	= require("express");
var app 		= module.exports = express();

app.post('/logout', authController.ensureAuthenticated, authController.authUnlink);

app.get('/me', authController.ensureAuthenticated, UserController.userMeGet);
app.put('/me', authController.ensureAuthenticated, UserController.userMePut);
// app.delete('/me', authController.ensureAuthenticated, UserController.userMeDelete);

app.get('/restaurant/:id', RestaurantController.restaurantGet); //authController.ensureAuthenticated, 
app.put('/restaurant/:id', RestaurantController.restaurantUpdate); //authController.ensureAuthenticated, 
app.post('/restaurant', RestaurantController.restaurantPost); //authController.ensureAuthenticated, 
app.delete('/restaurant/:id', RestaurantController.restaurantDelete); //authController.ensureAuthenticated, 

app.get('/restaurant/:id/menu/:menu/', RestaurantMenuController.menuGet); //authController.ensureAuthenticated, 
app.put('/restaurant/:id/menu/:menu/', RestaurantMenuController.menuUpdate); //authController.ensureAuthenticated, 
app.post('/restaurant/:id/menu', RestaurantMenuController.menuPost); //authController.ensureAuthenticated, 
app.delete('/restaurant/:id/menu/:menu/', RestaurantMenuController.menuDelete); //authController.ensureAuthenticated, 

app.get('/restaurant/:id/menu/:menu/category/:category', RestaurantMenuController.catGet); //authController.ensureAuthenticated, 
app.put('/restaurant/:id/menu/:menu/category/:category', RestaurantMenuController.catUpdate); //authController.ensureAuthenticated, 
app.post('/restaurant/:id/menu/:menu/category', RestaurantMenuController.catPost); //authController.ensureAuthenticated, 
app.delete('/restaurant/:id/menu/:menu/category/:category', RestaurantMenuController.catDelete); //authController.ensureAuthenticated,

app.get('/restaurant/:id/menu/:menu/category/:category/item/:item', RestaurantMenuController.itemGet); //authController.ensureAuthenticated, 
app.put('/restaurant/:id/menu/:menu/category/:category/item/:item', RestaurantMenuController.itemUpdate); //authController.ensureAuthenticated, 
app.post('/restaurant/:id/menu/:menu/category/:category/item', RestaurantMenuController.itemPost); //authController.ensureAuthenticated, 
app.delete('/restaurant/:id/menu/:menu/category/:category/item/:item', RestaurantMenuController.itemDelete); //authController.ensureAuthenticated,

app.get('/restaurant/:id/page/:page', RestaurantPagesController.restaurantGet); //authController.ensureAuthenticated, 
app.put('/restaurant/:id/page/:page', RestaurantPagesController.restaurantUpdate); //authController.ensureAuthenticated,
app.post('/restaurant/:id/page', RestaurantPagesController.restaurantPost); //authController.ensureAuthenticated, 
app.delete('/restaurant/:id/page/:page', RestaurantPagesController.restaurantDelete); //authController.ensureAuthenticated, 

app.get('/restaurant/:id/media/:media', RestaurantPagesController.restaurantGet); //authController.ensureAuthenticated, 
app.put('/restaurant/:id/media/:media', RestaurantPagesController.restaurantUpdate); //authController.ensureAuthenticated, 
app.post('/restaurant/:id/media', RestaurantPagesController.restaurantPost); //authController.ensureAuthenticated, 
app.delete('/restaurant/:id/media/:media', RestaurantPagesController.restaurantDelete); //authController.ensureAuthenticated, 


