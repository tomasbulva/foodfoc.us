var express = require('express');
var router = express.Router();

var authController = require('../Authentication/')

// middleware specific to this router
router.use(authController.ensureAuthenticated);

router.get('/me', authController.userMeGet);
router.put('/me', authController.userMePut);
router.post('/logout', authController.authUnlink);

module.exports = router;