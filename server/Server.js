var path = require('path');
var qs = require('querystring');

var async = require('async');
var bcrypt = require('bcryptjs');
var bodyParser = require('body-parser');
var colors = require('colors');
var cors = require('cors');
var express = require('express');
var logger = require('morgan');
var jwt = require('jwt-simple');
var moment = require('moment');
var mongoose = require('mongoose');
var request = require('request');

var User = require('./User/UserModel');

var config = require('./config');

mongoose.connect(config.MONGO_URI);
mongoose.connection.on('error', function(err) {
  console.error('Error: Could not connect to MongoDB. Did you forget to run `mongod`?'.red);
});

var app = express();

app.set('port', process.env.PORT || 3000);
app.use(cors());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// Force HTTPS on Heroku
if (app.get('env') === 'production') {
  app.use(function(req, res, next) {
    var protocol = req.get('x-forwarded-proto');
    protocol == 'https' ? next() : res.redirect('https://' + req.hostname + req.url);
  });
}

console.log('static path',path.join(__dirname, '../webclient'));

app.use(express.static(path.join(__dirname, '../webclient')));

// var loginRoute = require('./Routes/login');
// var apiRoutes = require('./Routes/');

// app.use('/auth', loginRoute);
// app.use('/api', apiRoutes);

app.use('/api', require("./Routes/"));
app.use('/auth', require("./Routes/login"));


/*
 |--------------------------------------------------------------------------
 | Start the Server
 |--------------------------------------------------------------------------
 */
app.listen(app.get('port'), function() {
  console.log('Express server listening on port ' + app.get('port'));
});