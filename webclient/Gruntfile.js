module.exports = function(grunt) {
	grunt.initConfig({
		jshint: {
			files: ['Gruntfile.js', 'app/*.js']
		},
		concat: {
			js: {
				src: ['bower_components/jquery/dist/jquery.js', 'bower_components/bootstrap/dist/js/bootstrap.js', 'bower_components/angular/angular.js'],
				dest: 'dist/libs.js'
			},
			jsangular: {
				src: ['bower_components/satellizer/satellizer.js','bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js', 'bower_components/angular-ui-router/release/angular-ui-router.min.js'],
				dest: 'dist/libs-angular.js'
			},
			css: {
				src: ['bower_components/bootstrap/dist/css/bootstrap.css', 'bower_components/bootstrap/dist/css/bootstrap-theme.css', 'bower_components/angular-bootstrap/ui-bootstrap-csp.css', 'less/style-source.css'],
				dest: 'dist/style.css'
			}
		},
		less: {
	      development: {
	        options: {
	            paths: ["bower_components/bootstrap/less"],
	            compress: true,
	            sourceMap: true,
	            sourceMapFilename: 'dist/style.css.map',
	            sourceMapURL: 'https://localhost/dist/style.css.map'
	        },
	        files: {
	            "bower_components/bootstrap/dist/css/bootstrap.css" : "bower_components/bootstrap/less/bootstrap.less",
	            "bower_components/bootstrap/dist/css/bootstrap-theme.css" : "bower_components/bootstrap/less/theme.less",
	            "less/style-source.css" : "less/general.less"
	        }   
	      } 
	    },
		uglify: {
			dist: {
				src: ['<%= concat.js.dest %>'],
				dest: 'dist/libs.min.js'
			}
		},
		watch: {
			files: '<%= jshint.files %>',
			tasks: 'jshint'
		},
		cafemocha: {
			testApi: {
				src: 'test/*.js',
				options: {
					ui: 'bdd',
					require: [
						'should'
					],
					reporter: 'spec'
				}
			},
      testSingleModule: {
          src: 'test/Routes.js',
          options: {
              ui: 'bdd',
              require: [
                  'should'
              ],
              reporter: 'spec'
          }
      }
		}
	});

	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks("grunt-contrib-less");
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-cafe-mocha');

	grunt.registerTask('test', ['jshint', 'cafemocha']);
    grunt.registerTask('singletest', ['jshint', 'cafemocha:testSingleModule']);
	grunt.registerTask('lint', ['jshint']);
	grunt.registerTask('default', ['jshint', /*'cafemocha',*/'less', 'concat', 'uglify']);
};
