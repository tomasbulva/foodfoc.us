var mongoose = require('mongoose');
var utils = require('../Utils/');
var ___ = require('underscore');
var async = require('async');

var restaurantMenuSchema = new mongoose.Schema({
  name: String,
  slug: String,
  intro: String,
  restaurant: {type: mongoose.Schema.Types.ObjectId, ref: 'Restaurant'}, 
  menuCats: [{type: mongoose.Schema.Types.ObjectId, ref: 'RestaurantMenuCat'}]
});

restaurantMenuSchema.pre('save', function(next) {
  var menu = this;
  //console.log('[restaurantMenuSchema - pre-save] menu',menu);
  if(menu.slug === undefined || menu.slug === '') {
    var slug = utils.slugify(menu.name);
    var allMenus;
    console.log('[restaurantMenuSchema - pre-save] slug',slug);
    async.series([
        function getExistingMenus(cb) { 
          var reg = new RegExp('^'+slug+'(_\d+)?', "i")
          module.exports.find({ slug: reg }, 'slug',function(err,existingMenus){
            if(err) { 
              console.log("err",err);
              cb(err);
            }else{
              console.log('find({ slug: reg } result:',existingMenus.length)
              allMenus = existingMenus;
              cb();
            }
          });
        },        
        function(cb) {
            console.log('[restaurantMenuSchema - pre-save] allMenus',allMenus);
            // no errors but menu doesn't exist
            // we're just slugify-ing title 
            if(!allMenus.length){
              menu.slug = slug;
              console.log('[restaurantMenuSchema - pre-save] This slug doesn\'t exist yet. Creating \''+slug+'\' slug!');
              return cb();
            }else{
              console.log('[restaurantMenuSchema - pre-save] Creating '+slug+'_'+allMenus.length+' slug!');
              menu.slug = slug+'_'+allMenus.length
              return cb();
            }
            // var existingSlug = ___.last(allMenus).slug;
            // var counter = existingSlug.lastIndexOf('_');
            // var number = Number(existingSlug.substr(counter));
            // console.log('[restaurantMenuSchema - pre-save] existingSlug',existingSlug);
            // console.log('[restaurantMenuSchema - pre-save] counter',counter);
            // if(counter == -1){
            //   //menu exits but has no counter at the end.
            //   //we're adding 1 at the end.
            //   menu.slug = slug+'_1';
            //   console.log('[restaurantMenuSchema - pre-save] menu.slug', menu.slug);
            //   cb();
            // }else{
            //   //menu with same slug exists and slug has counter at the end (_X)
            //   //we are converting the string into number and adding 1 to it.
            //   number++;
            //   menu.slug = slug+'_'+number;
            //   console.log('[restaurantMenuSchema - pre-save] menu.slug', menu.slug);
            //   cb();
            // }
        }
    ], function finish(err,result){
      console.log('[restaurantMenuSchema - pre-save] menu.slug', menu.slug);
      if (err){ 
        next(err); 
      }else{
        next();
      }
    });
  }
});

restaurantMenuSchema.pre('findOneAndUpdate', function(next) {
  var menu = this._update //.ownerDocument();
  if(menu.name !== undefined){
    console.log('[restaurantMenuSchema - pre-update] menu.name',menu.name);
    var slug = utils.slugify(menu.name);
    var allMenus;
      console.log('[restaurantMenuSchema - pre-update] slug',slug);
      async.series([
          function getExistingMenus(cb) { 
            module.exports.find({ slug: new RegExp('^'+slug+'$', "i") }, 'slug',function(err,existingMenus){
              if(err) { 
                console.log("err",err);
                cb(err);
              }else{
                allMenus = existingMenus;
                cb();
              }
            });
          },        
          function(cb) {
              console.log('[restaurantMenuSchema - pre-update] allMenus',allMenus);
              if(!allMenus.length){
                menu.slug = slug;
                console.log('[restaurantMenuSchema - pre-update] No same menus!');
                return cb();
              }else{
                menu.slug = slug+'_'+allMenus.length
              }
          }
      ], function finish(err,result){
        console.log('[restaurantMenuSchema - pre-update] menu', menu);
        if (err){
          console.log('[restaurantMenuSchema - pre-update] menu error',err);
          next(err); 
        }else{
          next();
        }
      });
}else{
  next();
}
});

module.exports = mongoose.model('RestaurantMenu', restaurantMenuSchema);