(function() {
	'use strict';  

	angular
		.module('app')
		.controller('HomeHeaderController', headerControllerFce)

		headerControllerFce.$inject = ['$scope','$rootScope', '$http', '$state', '$log', '$auth'];
		function headerControllerFce($scope, $rootScope, $http, $state, $log, $auth){
			$log.debug("*************** HomeHeaderController ****************");
			
			if($auth.isAuthenticated()){
				$state.go('home.dashboard');
			}else{
				$scope.authenticate = function(provider) {
					$auth.authenticate(provider).then(function(response) {
						$state.go('home.dashboard');
					}).catch(function(response) {
						$log.error("authentification failed", response);
					});
			    };
		  	}
		  	
		  	$scope.logout = function(provider) {
					console.log("[DashboardHeaderController] logout clicked")
					$auth.logout();
					$state.go('home');
			    };

		};

///////////// end closure  
})();