var express = require('express');
var router = express.Router();

var authController = require('../Authentication/')

router.route('/login').post(authController.authLogin);
router.route('/signup').post(authController.authSignup);
router.route('/google').post(authController.authGoogle);
router.route('/github').post(authController.authGithub);
router.route('/instagram').post(authController.authInstagram);
router.route('/linkedin').post(authController.authLinkedin);
router.route('/live').post(authController.authLive);
router.route('/facebook').post(authController.authFacebook);
router.route('/yahoo').post(authController.authYahoo);
router.route('/twitter').post(authController.authTwitter);
router.route('/foursquare').post(authController.authFoursquare);
router.route('/twitch').post(authController.authTwitch);

module.exports = router;